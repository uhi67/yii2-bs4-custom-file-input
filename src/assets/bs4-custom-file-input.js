$(function () {
	console.log('bs4-custom-file-input.js');

	// Transforms activeField inputs to the custom field template: "<div class='custom-file'>\n{input}\n{label}\n</div>\n{hint}\n{error}"
	$('input.bs4-custom-file-input').each(function() {
		const $label = $('label[for="'+this.id+'"]', $(this).parent()).addClass('custom-file-label');
		$(this).removeClass('bs4-custom-file-input').addClass('custom-file-input');
		const $div = $("<div class='custom-file'>").insertAfter(this);
		$(this).appendTo($div).after($label);
	});

	// Initialize the bs-custom-file-input plugin
	bsCustomFileInput.init();
});
