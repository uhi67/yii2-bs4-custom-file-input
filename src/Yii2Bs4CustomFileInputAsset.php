<?php

namespace uhi67\customFileInput;

class Yii2Bs4CustomFileInputAsset extends \yii\web\AssetBundle {
    public $sourcePath = __DIR__.'/assets';
    public $js = [
        'bs4-custom-file-input.js',
    ];
    public $depends = [
        BsCustomFileInputAsset::class,
    ];
}
