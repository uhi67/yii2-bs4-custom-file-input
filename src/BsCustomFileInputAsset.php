<?php

namespace uhi67\customFileInput;

class BsCustomFileInputAsset extends \yii\web\AssetBundle {
    public $sourcePath = '@npm/bs-custom-file-input';
    public $js = [
        'dist/bs-custom-file-input.min.js',
];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
