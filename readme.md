yii2-bs4-custom-file-input
==========================

A simple Yii2 wrapper of bootstrap4 custom-file-input plugin

Version v1.0 -- 2021-10-20

Prerequisites
-------------

    php >= 7.2
    yii2 >= 2.0.13
	bootstrap 4

Installation
------------

The preferred way to install this extension is through composer:

    composer require uhi67/yii2-bs4-custom-file-input

or clone from bitbucket

    git clone git@bitbucket.org:uhi67/yii2-bs4-custom-file-input.git

Usage
-----

### Usage in the view

```php
\uhi67\customFileInput\Yii2Bs4CustomFileInputAsset::register($this); // OR include in AppAsset globally

$form = \yii\bootstrap4\ActiveForm::begin();
echo $form->field($model, 'file')->fileInput(['class'=>'bs4-custom-file-input']); // The class indicates to be converted into custom-file-input structure. 
```

The rendered result is:

```html
<div class="form-group field-model-file required">
    <input type="hidden" name="Model[file]" value="">
    <div class="custom-file">
        <input type="file" id="model-file" class="form-control-file custom-file-input is-invalid" name="Model[file]" aria-required="true" aria-invalid="true">
        <label for="model-file" class="custom-file-label">label of field in Model</label>
    </div>
    <small class="form-text text-muted">hint of field in Model</small>
    <div class="invalid-feedback">Validation message</div>
</div>
```

## Change log

## v1.0

- first release
